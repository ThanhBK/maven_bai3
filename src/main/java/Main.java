import entity.Teacher;
import service.TeacherManager;

import java.time.LocalDate;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        // insert new Teacher
        Teacher newTeacher = new Teacher("GV10", "Nguyến Văn A", "Hà Nội", LocalDate.of(2001, 3, 3));
        TeacherManager.addTeacher(newTeacher);

        // find teacher has id = "DV03" with findTeacherById function
        System.out.println();
        Teacher teacher = TeacherManager.findTeacherById("GV05");
        if (teacher == null) {
            System.out.println("teacher not exists");
        } else {
            System.out.println(teacher);
        }

        // find teacher has id = "DV04" with findById function
        System.out.println();
        Teacher teacherDV04 = TeacherManager.findById("GV04");
        if (teacherDV04 == null) {
            System.out.println("teacher not exists");
        } else {
            System.out.println(teacherDV04);
        }

        //update teacher
        Teacher teacher1 = new Teacher();
        teacher1.setDia_chi("Nam Định");
        teacher1.setName("Update Teacher");
        teacher1.setBirthday(LocalDate.of(2013, 3, 4));
        TeacherManager.updateTeacher("GV04", teacher1);

        // find all teachers
        System.out.println();
        System.out.println("find all teachers");
        List<Teacher> teachers = TeacherManager.findAll();
        teachers.forEach(teacher2 -> System.out.println(teacher2));

        // delete
        TeacherManager.delete("GV05");

    }
}
