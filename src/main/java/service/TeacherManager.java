package service;

import entity.Teacher;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

public class TeacherManager {
    private final static EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence.createEntityManagerFactory("database_name_2");

    public static void addTeacher(Teacher newTeacher) {
        EntityManager entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction entityTransaction = null;
        // kiểm tra xem trong CSDL có tồn tại newTeacher có cùng id ko , nếu chưa có mới cho insert
        // ta cần kiểm tra xem newTeacher có id != null trước khi dùng hàm find để tránh NullpointException
        if(newTeacher.getId() != null && entityManager.find(Teacher.class, newTeacher.getId()) == null) {
            try {
                entityTransaction = entityManager.getTransaction();
                entityTransaction.begin();
                entityManager.persist(newTeacher);
                entityTransaction.commit();
            } catch (Exception ex) {
                if (entityTransaction != null) {
                    entityTransaction.rollback();
                }
                ex.printStackTrace();
            } finally {
                entityManager.close();
            }
        }
    }

    public static Teacher findTeacherById(String id) {
        EntityManager entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();
        Teacher teacher = entityManager.find(Teacher.class, id);
        return teacher;
    }

    public static void updateTeacher(String id, Teacher teacher) {
        EntityManager entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();
        Teacher newTeacher = entityManager.find(Teacher.class, id); // managed
        EntityTransaction entityTransaction = null;
        try {
            entityTransaction = entityManager.getTransaction();
            entityTransaction.begin();
            // thực hiện update newTeacher nên ta sao chép tất cả các thuộc tính của teacher vào newTeacher
            // ko cho phép sửa id nên ta sẽ ko cần setId
            newTeacher.setName(teacher.getName());
            newTeacher.setDia_chi(teacher.getDia_chi());
            newTeacher.setBirthday(teacher.getBirthday());
            entityTransaction.commit();
        } catch (Exception ex) {
            if (entityTransaction != null) {
                entityTransaction.rollback();
            }
            ex.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    public static void delete(String id) {
        EntityManager entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();
        Teacher teacher = entityManager.find(Teacher.class, id); // managed
        // nếu tìm thấy trong CSDL mới thực hiện xóa
        if(teacher != null) {
            EntityTransaction entityTransaction = null;
            try {
                entityTransaction = entityManager.getTransaction();
                entityTransaction.begin();
                entityManager.remove(teacher); // remove
                entityTransaction.commit();
            } catch (Exception ex) {
                if (entityTransaction != null) {
                    entityTransaction.rollback();
                }
                ex.printStackTrace();
            } finally {
                entityManager.close();
            }
        }
    }

    public static List<Teacher> findAll() {
        EntityManager entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();
        String query = "SELECT c FROM Teacher c"; // HQL
        TypedQuery<Teacher> typedQuery = entityManager.createQuery(query, Teacher.class);
        List<Teacher> teachers = new ArrayList<>();
        try {
            teachers = typedQuery.getResultList();
        } catch (NoResultException ex) {
            ex.printStackTrace();
        } finally {
            entityManager.close();
        }
        return teachers;
    }

    public static Teacher findById(String id) {
        EntityManager entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();
        String query = "SELECT c FROM Teacher c WHERE c.id = :gv_id"; // HQL
        TypedQuery<Teacher> typedQuery = entityManager.createQuery(query, Teacher.class);
        typedQuery.setParameter("gv_id", id);
        Teacher teacher = null;
        try {
            teacher = typedQuery.getSingleResult();
        } catch (NoResultException ex) {
            ex.printStackTrace();
        } finally {
            entityManager.close();
        }
        return teacher;
    }
}
