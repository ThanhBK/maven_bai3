package entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "giang_vien")
public class Teacher {
    public Teacher() {
    }

    public Teacher(String id, String name, String diaChi, LocalDate birthday) {
        this.id = id;
        this.name = name;
        this.dia_chi = diaChi;
        this.birthday = birthday;
    }

    // yêu cầu một primary key
    @Id
    @Column(name = "gv_id")
    private String id;

    @Column(name = "ho_ten", nullable = false)
    private String name;

    private String dia_chi;

    @Column(name = "ngay_sinh", nullable = false)
    private LocalDate birthday;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDia_chi() {
        return dia_chi;
    }

    public void setDia_chi(String dia_chi) {
        this.dia_chi = dia_chi;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return "entity.Teacher{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", dia_chi='" + dia_chi + '\'' +
                ", birthday=" + birthday +
                '}';
    }
}
